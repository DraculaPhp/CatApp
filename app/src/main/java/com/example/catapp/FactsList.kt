package com.example.catapp

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.catapp.DetailActivity.Companion.CAT_FACT_TEXT_TAG

class FactAdapter(private val facts: List<Cat>) : RecyclerView.Adapter<FactViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FactViewHolder {
        val rootView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.fact_item, parent, false)
        return FactViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: FactViewHolder, position: Int) {
        holder.bind(facts.get(position))
    }

    override fun getItemCount(): Int {
        return facts.size
    }

}

class FactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val textView: TextView = itemView.findViewById(R.id.textViewId)

    fun bind(cat: Cat) {
        textView.text = cat.getTitle()

        itemView.setOnClickListener {
            openDetailActivity(itemView.context, cat.getTitle())
        }
    }

    private fun openDetailActivity(context: Context, fact: String) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra(CAT_FACT_TEXT_TAG, fact)
        context.startActivity(intent)
    }
}