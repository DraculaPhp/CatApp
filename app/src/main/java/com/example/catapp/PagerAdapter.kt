package com.example.catapp

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class PagerAdapter (fragment: FragmentActivity) : FragmentStateAdapter(fragment) {

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                InternetFacts()
            }
            else -> {
                return ChosenFacts()
            }
        }
    }

    override fun getItemCount(): Int {
        return 2
    }
}