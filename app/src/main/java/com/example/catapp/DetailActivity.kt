package com.example.catapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import io.realm.Realm
import io.realm.RealmResults
import org.json.JSONObject

class DetailActivity : AppCompatActivity() {

    private val randomImageUrl = "https://aws.random.cat/meow"

    companion object {
        const val CAT_FACT_TEXT_TAG = "com.example.catapp.cat_fact_text_tag"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        setupActionBar()
        setText()

        val queue = Volley.newRequestQueue(this)
        getRandomImageFromServer(queue)

        val button: Button = findViewById(R.id.button)

        if (getFactFromDB().isEmpty()) {
            button.text = "Добавить в избранное"
            button.setOnClickListener {
                addFactIntoDB()
            }
        } else {
            button.text = "Удалить из избранного"
            button.setOnClickListener {
                removeFactFromDB()
            }
        }
    }

    private fun setupActionBar() {
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)

            title = "Detail"
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    private fun setText() {
        val text = intent?.extras?.getString(CAT_FACT_TEXT_TAG)
        val textViewDetailId: TextView = findViewById(R.id.textViewDetailId)
        textViewDetailId.text = text
    }

    private fun getRandomImageFromServer(queue: RequestQueue) {
        val stringRequest = StringRequest(
            Request.Method.GET,
            randomImageUrl,
            { response ->
                val circularProgressDrawable = CircularProgressDrawable(this)
                circularProgressDrawable.strokeWidth = 5f
                circularProgressDrawable.centerRadius = 30f
                circularProgressDrawable.start()
                val jsonObject = JSONObject(response).get("file")
                val catImageView: ImageView = findViewById(R.id.catImageId)
                Glide.with(this)
                    .load(jsonObject)
                    .placeholder(circularProgressDrawable)
                    .into(catImageView)
            },
            {
                Toast.makeText(this, "Ошибка запроса", Toast.LENGTH_SHORT).show()
            }
        )

        queue.add(stringRequest)
    }

    private fun addFactIntoDB() {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()

        val catFact: Cat = realm.createObject(Cat::class.java)
        catFact.setTitle(getTrimmedTitle())

        realm.commitTransaction()
        openMainActivity(this)
    }

    private fun removeFactFromDB() {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()

        val catFacts: RealmResults<Cat> = getFactFromDB()
        if (!catFacts.isEmpty()) {
            for (i in catFacts.size - 1 downTo 0) {
                catFacts[i]?.deleteFromRealm()
            }
        }

        realm.commitTransaction()
        openMainActivity(this)
    }

    private fun getFactFromDB(): RealmResults<Cat> {
        val realm = Realm.getDefaultInstance()

        return realm.where(Cat::class.java)
            .equalTo("title", getTrimmedTitle())
            .findAll()
    }

    private fun getTrimmedTitle(): String {
        val textView: TextView = findViewById(R.id.textViewDetailId)
        return textView.text.toString().trim()
    }

    private fun openMainActivity(context: Context) {
        val intent = Intent(context, MainActivity::class.java)
        context.startActivity(intent)
    }
}