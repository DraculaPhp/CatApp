package com.example.catapp

import io.realm.RealmObject
import io.realm.annotations.Required

open class Cat : RealmObject() {

    @Required

    private var title: String = ""

    fun setTitle(titleValue: String) {
        title = titleValue
    }

    fun getTitle(): String {
        return title
    }
}