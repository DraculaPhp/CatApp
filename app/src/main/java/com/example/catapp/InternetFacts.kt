package com.example.catapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray

class InternetFacts : Fragment() {

    private val url = "https://cat-fact.herokuapp.com/facts"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.internet_facts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val queue = Volley.newRequestQueue(super.getContext())
        getFactsFromServer(queue, view)
    }

    private fun getFactsFromServer(queue: RequestQueue, view: View) {
        val stringRequest = StringRequest(
            Request.Method.GET,
            url,
            { response ->
                val factsList = parseResponse(response)
                setList(factsList, view)
            },
            {
                Toast.makeText(super.getContext(), "Ошибка запроса", Toast.LENGTH_SHORT).show()
            }
        )

        queue.add(stringRequest)
    }

    private fun parseResponse(responseText: String): List<Cat> {
        val factsList: MutableList<Cat> = mutableListOf()
        val jsonArray = JSONArray(responseText)
        for (index in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(index)

            val factText = jsonObject.getString("text")
            val cat = Cat()
            cat.setTitle(factText)

            factsList.add(cat)
        }
        return factsList
    }

    private fun setList(facts: List<Cat>, view: View) {
        val adapter = FactAdapter(facts)
        val recyclerViewId: RecyclerView = view.findViewById(R.id.internetFactsRecyclerViewId)
        recyclerViewId.adapter = adapter

        val layoutManager = LinearLayoutManager(super.getContext())
        recyclerViewId.layoutManager = layoutManager
    }

}